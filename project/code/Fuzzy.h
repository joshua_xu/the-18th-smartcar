#ifndef __FUZZY_H__
#define __FUZZY_H__

#include "zf_common_headfile.h"

float Fuzzy_P(int  E,int EC);//第一个参数是误差，第二个是误差变化率
float Fuzzy_D(int  E,int EC);

#endif
