################################################################################
# MRS Version: {"version":"1.8.5","date":"2023/05/22"}
# 自动生成的文件。不要编辑！
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
F:/十八届智能车/比赛代码/project/code/ADC.c \
F:/十八届智能车/比赛代码/project/code/Camera.c \
F:/十八届智能车/比赛代码/project/code/Control.c \
F:/十八届智能车/比赛代码/project/code/FUZZY_PID_UCAS.c \
F:/十八届智能车/比赛代码/project/code/Fuzzy.c \
F:/十八届智能车/比赛代码/project/code/Gyroscope.c \
F:/十八届智能车/比赛代码/project/code/Island.c \
F:/十八届智能车/比赛代码/project/code/KEY.c \
F:/十八届智能车/比赛代码/project/code/PID.c \
F:/十八届智能车/比赛代码/project/code/UI.c \
F:/十八届智能车/比赛代码/project/code/ZwPlus_lib.c \
F:/十八届智能车/比赛代码/project/code/flash.c \
F:/十八届智能车/比赛代码/project/code/pico_link_ii.c \
F:/十八届智能车/比赛代码/project/code/test.c 

OBJS += \
./code/ADC.o \
./code/Camera.o \
./code/Control.o \
./code/FUZZY_PID_UCAS.o \
./code/Fuzzy.o \
./code/Gyroscope.o \
./code/Island.o \
./code/KEY.o \
./code/PID.o \
./code/UI.o \
./code/ZwPlus_lib.o \
./code/flash.o \
./code/pico_link_ii.o \
./code/test.o 

C_DEPS += \
./code/ADC.d \
./code/Camera.d \
./code/Control.d \
./code/FUZZY_PID_UCAS.d \
./code/Fuzzy.d \
./code/Gyroscope.d \
./code/Island.d \
./code/KEY.d \
./code/PID.d \
./code/UI.d \
./code/ZwPlus_lib.d \
./code/flash.d \
./code/pico_link_ii.d \
./code/test.d 


# Each subdirectory must supply rules for building sources it contributes
code/ADC.o: F:/十八届智能车/比赛代码/project/code/ADC.c
	@	@	riscv-none-embed-gcc -march=rv32imafc -mabi=ilp32f -msmall-data-limit=8 -mno-save-restore -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-common -pedantic -Wunused -Wuninitialized -Wall  -g -I"F:\十八届智能车\比赛代码\Libraries\doc" -I"F:\十八届智能车\比赛代码\libraries\sdk\Core" -I"F:\十八届智能车\比赛代码\libraries\sdk\Ld" -I"F:\十八届智能车\比赛代码\libraries\sdk\Peripheral" -I"F:\十八届智能车\比赛代码\libraries\sdk\Startup" -I"F:\十八届智能车\比赛代码\project\user\inc" -I"F:\十八届智能车\比赛代码\libraries\zf_common" -I"F:\十八届智能车\比赛代码\libraries\zf_device" -I"F:\十八届智能车\比赛代码\project\code" -I"F:\十八届智能车\比赛代码\libraries\zf_driver" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@	@
code/Camera.o: F:/十八届智能车/比赛代码/project/code/Camera.c
	@	@	riscv-none-embed-gcc -march=rv32imafc -mabi=ilp32f -msmall-data-limit=8 -mno-save-restore -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-common -pedantic -Wunused -Wuninitialized -Wall  -g -I"F:\十八届智能车\比赛代码\Libraries\doc" -I"F:\十八届智能车\比赛代码\libraries\sdk\Core" -I"F:\十八届智能车\比赛代码\libraries\sdk\Ld" -I"F:\十八届智能车\比赛代码\libraries\sdk\Peripheral" -I"F:\十八届智能车\比赛代码\libraries\sdk\Startup" -I"F:\十八届智能车\比赛代码\project\user\inc" -I"F:\十八届智能车\比赛代码\libraries\zf_common" -I"F:\十八届智能车\比赛代码\libraries\zf_device" -I"F:\十八届智能车\比赛代码\project\code" -I"F:\十八届智能车\比赛代码\libraries\zf_driver" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@	@
code/Control.o: F:/十八届智能车/比赛代码/project/code/Control.c
	@	@	riscv-none-embed-gcc -march=rv32imafc -mabi=ilp32f -msmall-data-limit=8 -mno-save-restore -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-common -pedantic -Wunused -Wuninitialized -Wall  -g -I"F:\十八届智能车\比赛代码\Libraries\doc" -I"F:\十八届智能车\比赛代码\libraries\sdk\Core" -I"F:\十八届智能车\比赛代码\libraries\sdk\Ld" -I"F:\十八届智能车\比赛代码\libraries\sdk\Peripheral" -I"F:\十八届智能车\比赛代码\libraries\sdk\Startup" -I"F:\十八届智能车\比赛代码\project\user\inc" -I"F:\十八届智能车\比赛代码\libraries\zf_common" -I"F:\十八届智能车\比赛代码\libraries\zf_device" -I"F:\十八届智能车\比赛代码\project\code" -I"F:\十八届智能车\比赛代码\libraries\zf_driver" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@	@
code/FUZZY_PID_UCAS.o: F:/十八届智能车/比赛代码/project/code/FUZZY_PID_UCAS.c
	@	@	riscv-none-embed-gcc -march=rv32imafc -mabi=ilp32f -msmall-data-limit=8 -mno-save-restore -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-common -pedantic -Wunused -Wuninitialized -Wall  -g -I"F:\十八届智能车\比赛代码\Libraries\doc" -I"F:\十八届智能车\比赛代码\libraries\sdk\Core" -I"F:\十八届智能车\比赛代码\libraries\sdk\Ld" -I"F:\十八届智能车\比赛代码\libraries\sdk\Peripheral" -I"F:\十八届智能车\比赛代码\libraries\sdk\Startup" -I"F:\十八届智能车\比赛代码\project\user\inc" -I"F:\十八届智能车\比赛代码\libraries\zf_common" -I"F:\十八届智能车\比赛代码\libraries\zf_device" -I"F:\十八届智能车\比赛代码\project\code" -I"F:\十八届智能车\比赛代码\libraries\zf_driver" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@	@
code/Fuzzy.o: F:/十八届智能车/比赛代码/project/code/Fuzzy.c
	@	@	riscv-none-embed-gcc -march=rv32imafc -mabi=ilp32f -msmall-data-limit=8 -mno-save-restore -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-common -pedantic -Wunused -Wuninitialized -Wall  -g -I"F:\十八届智能车\比赛代码\Libraries\doc" -I"F:\十八届智能车\比赛代码\libraries\sdk\Core" -I"F:\十八届智能车\比赛代码\libraries\sdk\Ld" -I"F:\十八届智能车\比赛代码\libraries\sdk\Peripheral" -I"F:\十八届智能车\比赛代码\libraries\sdk\Startup" -I"F:\十八届智能车\比赛代码\project\user\inc" -I"F:\十八届智能车\比赛代码\libraries\zf_common" -I"F:\十八届智能车\比赛代码\libraries\zf_device" -I"F:\十八届智能车\比赛代码\project\code" -I"F:\十八届智能车\比赛代码\libraries\zf_driver" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@	@
code/Gyroscope.o: F:/十八届智能车/比赛代码/project/code/Gyroscope.c
	@	@	riscv-none-embed-gcc -march=rv32imafc -mabi=ilp32f -msmall-data-limit=8 -mno-save-restore -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-common -pedantic -Wunused -Wuninitialized -Wall  -g -I"F:\十八届智能车\比赛代码\Libraries\doc" -I"F:\十八届智能车\比赛代码\libraries\sdk\Core" -I"F:\十八届智能车\比赛代码\libraries\sdk\Ld" -I"F:\十八届智能车\比赛代码\libraries\sdk\Peripheral" -I"F:\十八届智能车\比赛代码\libraries\sdk\Startup" -I"F:\十八届智能车\比赛代码\project\user\inc" -I"F:\十八届智能车\比赛代码\libraries\zf_common" -I"F:\十八届智能车\比赛代码\libraries\zf_device" -I"F:\十八届智能车\比赛代码\project\code" -I"F:\十八届智能车\比赛代码\libraries\zf_driver" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@	@
code/Island.o: F:/十八届智能车/比赛代码/project/code/Island.c
	@	@	riscv-none-embed-gcc -march=rv32imafc -mabi=ilp32f -msmall-data-limit=8 -mno-save-restore -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-common -pedantic -Wunused -Wuninitialized -Wall  -g -I"F:\十八届智能车\比赛代码\Libraries\doc" -I"F:\十八届智能车\比赛代码\libraries\sdk\Core" -I"F:\十八届智能车\比赛代码\libraries\sdk\Ld" -I"F:\十八届智能车\比赛代码\libraries\sdk\Peripheral" -I"F:\十八届智能车\比赛代码\libraries\sdk\Startup" -I"F:\十八届智能车\比赛代码\project\user\inc" -I"F:\十八届智能车\比赛代码\libraries\zf_common" -I"F:\十八届智能车\比赛代码\libraries\zf_device" -I"F:\十八届智能车\比赛代码\project\code" -I"F:\十八届智能车\比赛代码\libraries\zf_driver" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@	@
code/KEY.o: F:/十八届智能车/比赛代码/project/code/KEY.c
	@	@	riscv-none-embed-gcc -march=rv32imafc -mabi=ilp32f -msmall-data-limit=8 -mno-save-restore -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-common -pedantic -Wunused -Wuninitialized -Wall  -g -I"F:\十八届智能车\比赛代码\Libraries\doc" -I"F:\十八届智能车\比赛代码\libraries\sdk\Core" -I"F:\十八届智能车\比赛代码\libraries\sdk\Ld" -I"F:\十八届智能车\比赛代码\libraries\sdk\Peripheral" -I"F:\十八届智能车\比赛代码\libraries\sdk\Startup" -I"F:\十八届智能车\比赛代码\project\user\inc" -I"F:\十八届智能车\比赛代码\libraries\zf_common" -I"F:\十八届智能车\比赛代码\libraries\zf_device" -I"F:\十八届智能车\比赛代码\project\code" -I"F:\十八届智能车\比赛代码\libraries\zf_driver" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@	@
code/PID.o: F:/十八届智能车/比赛代码/project/code/PID.c
	@	@	riscv-none-embed-gcc -march=rv32imafc -mabi=ilp32f -msmall-data-limit=8 -mno-save-restore -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-common -pedantic -Wunused -Wuninitialized -Wall  -g -I"F:\十八届智能车\比赛代码\Libraries\doc" -I"F:\十八届智能车\比赛代码\libraries\sdk\Core" -I"F:\十八届智能车\比赛代码\libraries\sdk\Ld" -I"F:\十八届智能车\比赛代码\libraries\sdk\Peripheral" -I"F:\十八届智能车\比赛代码\libraries\sdk\Startup" -I"F:\十八届智能车\比赛代码\project\user\inc" -I"F:\十八届智能车\比赛代码\libraries\zf_common" -I"F:\十八届智能车\比赛代码\libraries\zf_device" -I"F:\十八届智能车\比赛代码\project\code" -I"F:\十八届智能车\比赛代码\libraries\zf_driver" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@	@
code/UI.o: F:/十八届智能车/比赛代码/project/code/UI.c
	@	@	riscv-none-embed-gcc -march=rv32imafc -mabi=ilp32f -msmall-data-limit=8 -mno-save-restore -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-common -pedantic -Wunused -Wuninitialized -Wall  -g -I"F:\十八届智能车\比赛代码\Libraries\doc" -I"F:\十八届智能车\比赛代码\libraries\sdk\Core" -I"F:\十八届智能车\比赛代码\libraries\sdk\Ld" -I"F:\十八届智能车\比赛代码\libraries\sdk\Peripheral" -I"F:\十八届智能车\比赛代码\libraries\sdk\Startup" -I"F:\十八届智能车\比赛代码\project\user\inc" -I"F:\十八届智能车\比赛代码\libraries\zf_common" -I"F:\十八届智能车\比赛代码\libraries\zf_device" -I"F:\十八届智能车\比赛代码\project\code" -I"F:\十八届智能车\比赛代码\libraries\zf_driver" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@	@
code/ZwPlus_lib.o: F:/十八届智能车/比赛代码/project/code/ZwPlus_lib.c
	@	@	riscv-none-embed-gcc -march=rv32imafc -mabi=ilp32f -msmall-data-limit=8 -mno-save-restore -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-common -pedantic -Wunused -Wuninitialized -Wall  -g -I"F:\十八届智能车\比赛代码\Libraries\doc" -I"F:\十八届智能车\比赛代码\libraries\sdk\Core" -I"F:\十八届智能车\比赛代码\libraries\sdk\Ld" -I"F:\十八届智能车\比赛代码\libraries\sdk\Peripheral" -I"F:\十八届智能车\比赛代码\libraries\sdk\Startup" -I"F:\十八届智能车\比赛代码\project\user\inc" -I"F:\十八届智能车\比赛代码\libraries\zf_common" -I"F:\十八届智能车\比赛代码\libraries\zf_device" -I"F:\十八届智能车\比赛代码\project\code" -I"F:\十八届智能车\比赛代码\libraries\zf_driver" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@	@
code/flash.o: F:/十八届智能车/比赛代码/project/code/flash.c
	@	@	riscv-none-embed-gcc -march=rv32imafc -mabi=ilp32f -msmall-data-limit=8 -mno-save-restore -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-common -pedantic -Wunused -Wuninitialized -Wall  -g -I"F:\十八届智能车\比赛代码\Libraries\doc" -I"F:\十八届智能车\比赛代码\libraries\sdk\Core" -I"F:\十八届智能车\比赛代码\libraries\sdk\Ld" -I"F:\十八届智能车\比赛代码\libraries\sdk\Peripheral" -I"F:\十八届智能车\比赛代码\libraries\sdk\Startup" -I"F:\十八届智能车\比赛代码\project\user\inc" -I"F:\十八届智能车\比赛代码\libraries\zf_common" -I"F:\十八届智能车\比赛代码\libraries\zf_device" -I"F:\十八届智能车\比赛代码\project\code" -I"F:\十八届智能车\比赛代码\libraries\zf_driver" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@	@
code/pico_link_ii.o: F:/十八届智能车/比赛代码/project/code/pico_link_ii.c
	@	@	riscv-none-embed-gcc -march=rv32imafc -mabi=ilp32f -msmall-data-limit=8 -mno-save-restore -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-common -pedantic -Wunused -Wuninitialized -Wall  -g -I"F:\十八届智能车\比赛代码\Libraries\doc" -I"F:\十八届智能车\比赛代码\libraries\sdk\Core" -I"F:\十八届智能车\比赛代码\libraries\sdk\Ld" -I"F:\十八届智能车\比赛代码\libraries\sdk\Peripheral" -I"F:\十八届智能车\比赛代码\libraries\sdk\Startup" -I"F:\十八届智能车\比赛代码\project\user\inc" -I"F:\十八届智能车\比赛代码\libraries\zf_common" -I"F:\十八届智能车\比赛代码\libraries\zf_device" -I"F:\十八届智能车\比赛代码\project\code" -I"F:\十八届智能车\比赛代码\libraries\zf_driver" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@	@
code/test.o: F:/十八届智能车/比赛代码/project/code/test.c
	@	@	riscv-none-embed-gcc -march=rv32imafc -mabi=ilp32f -msmall-data-limit=8 -mno-save-restore -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-common -pedantic -Wunused -Wuninitialized -Wall  -g -I"F:\十八届智能车\比赛代码\Libraries\doc" -I"F:\十八届智能车\比赛代码\libraries\sdk\Core" -I"F:\十八届智能车\比赛代码\libraries\sdk\Ld" -I"F:\十八届智能车\比赛代码\libraries\sdk\Peripheral" -I"F:\十八届智能车\比赛代码\libraries\sdk\Startup" -I"F:\十八届智能车\比赛代码\project\user\inc" -I"F:\十八届智能车\比赛代码\libraries\zf_common" -I"F:\十八届智能车\比赛代码\libraries\zf_device" -I"F:\十八届智能车\比赛代码\project\code" -I"F:\十八届智能车\比赛代码\libraries\zf_driver" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@	@

