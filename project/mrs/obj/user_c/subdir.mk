################################################################################
# MRS Version: {"version":"1.8.5","date":"2023/05/22"}
# 自动生成的文件。不要编辑！
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
F:/十八届智能车/比赛代码/project/user/src/isr.c \
F:/十八届智能车/比赛代码/project/user/src/main.c 

OBJS += \
./user_c/isr.o \
./user_c/main.o 

C_DEPS += \
./user_c/isr.d \
./user_c/main.d 


# Each subdirectory must supply rules for building sources it contributes
user_c/isr.o: F:/十八届智能车/比赛代码/project/user/src/isr.c
	@	@	riscv-none-embed-gcc -march=rv32imafc -mabi=ilp32f -msmall-data-limit=8 -mno-save-restore -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-common -pedantic -Wunused -Wuninitialized -Wall  -g -I"F:\十八届智能车\比赛代码\Libraries\doc" -I"F:\十八届智能车\比赛代码\libraries\sdk\Core" -I"F:\十八届智能车\比赛代码\libraries\sdk\Ld" -I"F:\十八届智能车\比赛代码\libraries\sdk\Peripheral" -I"F:\十八届智能车\比赛代码\libraries\sdk\Startup" -I"F:\十八届智能车\比赛代码\project\user\inc" -I"F:\十八届智能车\比赛代码\libraries\zf_common" -I"F:\十八届智能车\比赛代码\libraries\zf_device" -I"F:\十八届智能车\比赛代码\project\code" -I"F:\十八届智能车\比赛代码\libraries\zf_driver" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@	@
user_c/main.o: F:/十八届智能车/比赛代码/project/user/src/main.c
	@	@	riscv-none-embed-gcc -march=rv32imafc -mabi=ilp32f -msmall-data-limit=8 -mno-save-restore -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-common -pedantic -Wunused -Wuninitialized -Wall  -g -I"F:\十八届智能车\比赛代码\Libraries\doc" -I"F:\十八届智能车\比赛代码\libraries\sdk\Core" -I"F:\十八届智能车\比赛代码\libraries\sdk\Ld" -I"F:\十八届智能车\比赛代码\libraries\sdk\Peripheral" -I"F:\十八届智能车\比赛代码\libraries\sdk\Startup" -I"F:\十八届智能车\比赛代码\project\user\inc" -I"F:\十八届智能车\比赛代码\libraries\zf_common" -I"F:\十八届智能车\比赛代码\libraries\zf_device" -I"F:\十八届智能车\比赛代码\project\code" -I"F:\十八届智能车\比赛代码\libraries\zf_driver" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@	@

